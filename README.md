# MedTech Prototyping Skills (BME290L)
This repository serves as the default collection for all resources associated
with the Duke BME MedTech Prototyping Skills (BME290L) course.

## Tasks at the Start of Each Semester
* [X] Update syllabus & schedule
* [ ] Setup Sakai (https://kits.duke.edu)
    - [X] Setup new site
    - [X] Upload resources
    - [X] add TAs
* [ ] Setup Teams (https://kits.duke.edu)
* [X] Setup Gradescope
* [X] Setup Onshape
* [X] Chesterfield \& POD access
* [X] Panopto (lecture recording)

## TA Responsibilities
* Preview all assignments (collectively) to find errors / confusion
* Help grade via Gradescope
* Attend lab period to help students or hold office hours (3 hours total)

## Contributors
* Mark Palmeri

## Licensing
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Medical Electrical Equipment</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/mlp6/" property="cc:attributionName" rel="cc:attributionURL">Mark L. Palmeri</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.en_US">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.oit.duke.edu/mlp6/Medical-Electrical-Equipment" rel="dct:source">https://gitlab.oit.duke.edu/mlp6/Medical-Electrical-Equipment</a>.
